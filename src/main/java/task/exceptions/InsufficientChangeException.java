package task.exceptions;

public class InsufficientChangeException extends RuntimeException {
    public InsufficientChangeException(String errorMessage){
        super(errorMessage);
    }
}
