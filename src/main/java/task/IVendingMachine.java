package task;

import java.util.Collection;

public interface IVendingMachine {
    void insertCoin(int amount);
    Collection<Integer> getChange(int value, Strategy strategy);
    Collection<Integer> getChange(int value);
}
