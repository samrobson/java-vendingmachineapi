package task.helpers;

import task.Strategy;
import task.exceptions.InsufficientChangeException;

import java.util.*;
import java.util.stream.Collectors;

// Class responsible for calculating appropriate change for a given strategy
public class ChangeCalculator {
    // Default to the QUICK strategy
    public List<Integer> getChange(int amount, Map<Integer, Integer> coins, Strategy strategy){
        switch(strategy){
            case LEAST: return getLeastChange(amount, coins);
            case BALANCED: return getBalancedChange(amount, coins);
            case QUICK:
            default: return getQuickChange(amount, coins);
        }
    }

    // Get change using the least number of coins possible
    private List<Integer> getLeastChange(int amount, Map<Integer, Integer> coins) {
        List<HashMap<Integer, Integer>> combinations = getCombinations(amount, coins);
        HashMap<Integer, Integer> leastChangeCombination = getLeast(combinations);
        List<Integer> asList = convertChangeMapToList(leastChangeCombination);
        updateCoins(coins, asList);
        return asList;
    }

    // Get change using the least number of coins, while also not consuming all of any denomination, if possible
    private List<Integer> getBalancedChange(int amount, Map<Integer, Integer> coins) {
        List<HashMap<Integer, Integer>> combinations = getCombinations(amount, coins);

        List<HashMap<Integer, Integer>> balancedCombinations = combinations.stream().filter(changeMap -> {
            for (int denom : changeMap.keySet()){
                // Disregard combinations that use all coins of one denomination
                if (changeMap.get(denom).equals(coins.get(denom))) { return false; }
            }
            return true;
        }).collect(Collectors.toList());

        // If a balanced approach is not possible, revert to just using the LEAST strategy
        HashMap<Integer, Integer> leastChangeCombination = !balancedCombinations.isEmpty() ?
                getLeast(balancedCombinations) : getLeast(combinations);
        List<Integer> asList = convertChangeMapToList(leastChangeCombination);
        updateCoins(coins, asList);
        return asList;
    }

    private List<HashMap<Integer, Integer>> getCombinations(int amount, Map<Integer, Integer> coins) {
        Set<Integer> denominations = coins.keySet();

        // Build the map of combinations for each amount of change
        Map<Integer, List<HashMap<Integer, Integer>>> combinations = new HashMap<>();
        for (int i = 0; i <= amount; i++) {
            combinations.put(i, new ArrayList<>());
        }
        combinations.get(0).add(new HashMap<>());

        for (int denom : denominations) {
            for (int i = 1; i < amount + 1; i++) {
                List<HashMap<Integer, Integer>> currentCombinations = combinations.get(i);
                if (denom <= i) {
                    // If the denomination can be used as change, add it to all collections of combinations for i - denom
                    for (HashMap<Integer, Integer> prevCombinations : combinations.get(i - denom)) {
                        HashMap<Integer, Integer> oldMap = new HashMap<>(prevCombinations);
                        int quantity = oldMap.getOrDefault(denom, 0);
                        // Only add the denomination to the combination if we have enough coins
                        if (quantity + 1 <= coins.get(denom)){
                            oldMap.put(denom, quantity + 1);
                            currentCombinations.add(oldMap);
                        }
                    }
                }
            }
        }
        if (combinations.get(amount).isEmpty()){
            throw new InsufficientChangeException(String.format("Insufficient change for %d", amount));
        }
        return combinations.get(amount);
    }

    // Least change used can be found by summing the values in the denomination - quantity map
    private HashMap<Integer, Integer> getLeast(List<HashMap<Integer, Integer>> combinations) {
        return combinations.stream().min(
                Comparator.comparingInt(map -> map.values().stream().mapToInt(Integer::intValue).sum())).orElse(null);
    }

    // Greedy implementation, this will be sufficient in most cases, given how currencies are designed
    private List<Integer> getQuickChange(int amount, Map<Integer, Integer> coins) {
        // Use a copy of the map, so we don't modify the original before finishing the transaction
        HashMap<Integer, Integer> coinsCopy = new HashMap<>(coins);
        List<Integer> change = new ArrayList<>();
        int changeToGo = amount;
        // Order the list of denominations in the vending machine, to proceed with the largest first
        List<Integer> largestDenomsFirst = coinsCopy.keySet().stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        for (int i = 0; i < largestDenomsFirst.size();){
            if (changeToGo == 0){ break; }
            int denom = largestDenomsFirst.get(i);
            int quantity = coinsCopy.get(denom);
            // If the denomination is too large, or we have no such coins left, move to the next coin
            if (denom > changeToGo || quantity == 0){
                i++;
            }
            else {
                changeToGo -= denom;
                coinsCopy.put(denom, quantity-1);
                change.add(denom);
            }
        }
        if (changeToGo != 0){
            throw new InsufficientChangeException(String.format("Insufficient change for %d", amount));
        }
        updateCoins(coins, change);
        return change;
    }

    private List<Integer> convertChangeMapToList(HashMap<Integer, Integer> leastChangeCombination) {
        List<Integer> asList = new ArrayList<>();
        if (leastChangeCombination != null){
            leastChangeCombination.forEach((coin, quantity) -> {
                for (int i = 0; i < quantity; i++){
                    asList.add(coin);
                }
            });
        }
        return asList;
    }

    // Update the coins in the vending machine, once the transaction has completed
    private void updateCoins(Map<Integer, Integer> coins, List<Integer> change) {
        change.forEach(coin -> {
            int quantity = coins.get(coin);
            coins.put(coin, quantity-1);
        });
    }
}
