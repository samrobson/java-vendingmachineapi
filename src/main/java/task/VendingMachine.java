package task;

import task.helpers.ChangeCalculator;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class VendingMachine implements IVendingMachine {
    // Map of denominations to their quantity, present in the machine
    private final HashMap<Integer, Integer> coins;
    private final ChangeCalculator changeCalculator;

    public VendingMachine(Map<Integer, Integer> initialCoins) {
        this.changeCalculator = new ChangeCalculator();
        this.coins = new HashMap<>(initialCoins);
    }

    public Map<Integer, Integer> getCoins(){
        return coins;
    }

    public void insertCoin(int denomination){
        coins.merge(denomination, 1, Integer::sum);
    }

    public Collection<Integer> getChange(int value, Strategy strategy){
        return changeCalculator.getChange(value, coins, strategy);
    }

    // Default to the QUICK strategy
    public Collection<Integer> getChange(int value){
        return changeCalculator.getChange(value, coins, Strategy.QUICK);
    }
}
