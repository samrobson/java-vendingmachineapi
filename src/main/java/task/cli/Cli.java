package task.cli;

import task.Strategy;
import task.VendingMachine;
import task.exceptions.InsufficientChangeException;

import java.util.HashMap;
import java.util.Scanner;

import static java.lang.System.out;

public class Cli {

    // Basic CLI to facilitate manual testing
    public static void main(String[] args) {
        Cli cli = new Cli();
        HashMap<Integer, Integer> initialState = cli.buildInitialState();
        VendingMachine vendingMachine = new VendingMachine(initialState);
        cli.startCli(vendingMachine);
    }

    private void startCli(VendingMachine vendingMachine) {
        out.println("Initialised the vending machine with the following data:");
        out.println(vendingMachine.getCoins());
        Scanner scanner = new Scanner(System.in);
        boolean running = true;
        while (running) {
            out.println("--- Available Options ---");
            out.println("Enter >=1 to insert a coin with that value");
            out.println("Enter 0 to start request mode");
            out.println("Enter -1 to exit");
            int num = scanner.nextInt();
            switch(num){
                case 0:
                    out.println("--- Available Options ---");
                    out.println("Enter 0 to use the QUICK strategy");
                    out.println("Enter 1 to use the BALANCED strategy");
                    out.println("Enter 2 to use the LEAST strategy");
                    out.println("Enter -1 to exit");
                    int strategy = scanner.nextInt();
                    out.println("Enter the change you require");
                    try {
                        switch (strategy) {
                            case 0: out.println(vendingMachine.getChange(scanner.nextInt(), Strategy.QUICK)); break;
                            case 1: out.println(vendingMachine.getChange(scanner.nextInt(), Strategy.BALANCED)); break;
                            case 2: out.println(vendingMachine.getChange(scanner.nextInt(), Strategy.LEAST)); break;
                        } break;
                    }
                    catch (InsufficientChangeException err){
                        out.println(err.getMessage());
                        out.println(vendingMachine.getCoins());
                        break;
                    }
                case -1: running = false; break;
                default:
                    vendingMachine.insertCoin(num);
                    out.println(String.format("%s inserted", num));
                    out.println(vendingMachine.getCoins()); break;
            }
        }
    }

    // Initialize the vending machine with some useful dummy data
    private HashMap<Integer, Integer> buildInitialState() {
        HashMap<Integer, Integer> initialState = new HashMap<>();
        initialState.put(1, 10);
        initialState.put(2, 5);
        initialState.put(5, 3);
        initialState.put(10, 1);
        return initialState;
    }
}
