package task;

import task.exceptions.InsufficientChangeException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestChangeCalculator {
    VendingMachine vendingMachine;

    @BeforeEach
    public void setup(){
        HashMap<Integer, Integer> initialState = new HashMap<>();
        initialState.put(1, 10);
        initialState.put(2, 5);
        initialState.put(5, 3);
        initialState.put(10, 1);

        vendingMachine = new VendingMachine(initialState);
    }

    @Test
    public void testCorrectChangeQuick(){
        Collection<Integer> change = vendingMachine.getChange(21, Strategy.QUICK);
        assertEquals(change, new ArrayList<>(Arrays.asList(10, 5, 5, 1)));
    }

    @Test
    public void testCorrectChangeLeast(){
        vendingMachine.insertCoin(9);
        vendingMachine.insertCoin(9);
        Collection<Integer> change = vendingMachine.getChange(18, Strategy.LEAST);
        assertEquals(change, new ArrayList<>(Arrays.asList(9, 9)));
    }

    @Test
    public void testCorrectChangeBalanced(){
        Collection<Integer> change = vendingMachine.getChange(12, Strategy.BALANCED);
        assertEquals(change, new ArrayList<>(Arrays.asList(5, 5, 2)));
    }

    @Test
    public void testCorrectChangeQuickMultiple(){
        Collection<Integer> firstChange = vendingMachine.getChange(14, Strategy.QUICK);
        assertEquals(firstChange, new ArrayList<>(Arrays.asList(10, 2, 2)));

        Collection<Integer> secondChange = vendingMachine.getChange(14, Strategy.QUICK);
        assertEquals(secondChange, new ArrayList<>(Arrays.asList(5, 5, 2, 2)));

        Collection<Integer> thirdChange = vendingMachine.getChange(14, Strategy.QUICK);
        assertEquals(thirdChange, new ArrayList<>(Arrays.asList(5, 2, 1, 1, 1, 1, 1, 1, 1)));
    }

    @Test
    public void testCorrectChangeAfterInserting(){
        assertThrows(InsufficientChangeException.class, () -> vendingMachine.getChange(50));

        vendingMachine.insertCoin(20);

        Collection<Integer> secondChange = vendingMachine.getChange(50, Strategy.QUICK);
        assertEquals(secondChange, new ArrayList<>(Arrays.asList(20, 10, 5, 5, 5, 2, 2, 1)));
    }

    @Test
    public void testThrowsInsufficientChangeException(){
        assertThrows(InsufficientChangeException.class, () -> vendingMachine.getChange(50));
    }

    // Even though we can typically get the correct change with the QUICK strategy, in the case of unusual denominations we can encounter problems
    @Test
    public void testQuickStrategyThrowsWithUnusualDenominations(){
        HashMap<Integer, Integer> initialState = new HashMap<>();
        initialState.put(2, 10);
        initialState.put(4, 5);
        initialState.put(7, 3);
        initialState.put(10, 1);

        VendingMachine vMachine = new VendingMachine(initialState);
        assertThrows(InsufficientChangeException.class, () -> vMachine.getChange(18, Strategy.QUICK));
    }
}
