# Vending Machine API

The goal for this project was to build a change calculation API for a vending machine.

## Design
- For this task, I wrote a library that provides an abstraction for a change calculation algorithm.
- I chose to represent the assumed requirements via the `IVendingMachine` interface, add an implementation with the `VendingMachine` class, and include a helper class to facilitate all the calculations in `ChangeCalculator`
- I designed this API to be unopinionated and accept denominations of any kind.
- The constructor for the `VendingMachine` requires a map of coins to their quantity, and can accept individual coin insertions.

## Change calculation strategies
- I wanted to accommodate different change calculation strategies, as detailed below; it isn't clear to me which is the best, and I think that flexibility is an important feature of any API.
1. Quick strategy
    - Iterate through the denominations, largest first, inserting them into a list of change required if the coin < amountRemaining
    - This can fail, though. Consider calculating the change for 16p, if the vending machine possessed `[10, 5, 2, 2, 2]`
2. Least coins used strategy: 
    - Build a map of every possible combination of coins to make the required change, given available coins
    - This will work with unusual denominations, and avoids the major flaw with the quick strategy, but it is computationally expensive. Additionally, it can lead to cases where certain denominations are emptied unnecessarily, potentially frustrating future change requests.
3. Balanced strategy
    - Build a map of every possible combination of coins to make the required change, given available coins
    - Search for a combination that doesn't empty any one denomination
    - If no such options are available, revert to using the combination that uses the least coins

Improvements
- Cache the combinations map used as part of the least coins and balanced strategies. 


## How to Run
### Tests
- You can run the tests via the gradle `test` task - `./gradlew clean test`

### CLI
- You can manually test the API through a basic CLI, with either `Docker` (recommended) or `gradle`
#### Docker
1. Build the image
```
    docker build --tag vendingmachine-api:1.0 .
```
2. Run the image, and experiment with the basic CLI
```
    docker run -it vendingmachine-api:1.0
```
#### Gradle
1. Build the project using gradlew from the root directory `./gradlew build`
2. Run the CLI `./gradlew run `